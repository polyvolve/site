import * as React from "react"
import cx from "classnames"
import Link from "next/link"
import staticWrapper from "../lib/static"

import * as linkedinSvg from "../../public/linkedin.svg"
import * as twitterSvg from "../../public/twitter.svg"
import { SITE_NAME } from "../constants/env"

import { Logo, NavigationItem } from "polyvolve-ui"
import { footerStyle } from "../lib/reexports"
import { Icon } from "polyvolve-ui/lib/icons"

const Footer: React.FunctionComponent = props => (
  <footer className={footerStyle.footer}>
    <div className={footerStyle.footerMenu}>
      <div className={footerStyle.footerMenuItem}>
        <h4>Product</h4>
        <div>
          <ul>
            <NavigationItem name="Benefits" />
            <NavigationItem name="How It Works" />
            <NavigationItem name="Pricing" />
            <NavigationItem name="Roadmap" />
          </ul>
        </div>
      </div>
      <div className={footerStyle.footerMenuItem}>
        <h4>Company</h4>
        <div>
          <ul>
            <NavigationItem name="About Us" />
            <NavigationItem name="Blog" />
            <NavigationItem name="Careers" />
          </ul>
        </div>
      </div>
      <div className={footerStyle.footerMenuItem}>
        <h4>Get Started</h4>
        <div>
          <ul>
            <NavigationItem name="Request a Free Trial" />
            <NavigationItem name="Contact us" />
          </ul>
        </div>
      </div>
    </div>
    <div className={footerStyle.footerBar}>
      <div className={footerStyle.footerTitle}>
        <Logo size={24} />
      </div>
      <div>
        <ul className={footerStyle.discPrivTosBar}>
          <NavigationItem name="Disclaimer" />
          <NavigationItem name="Privacy" />
          <NavigationItem name="Terms of Service" />
        </ul>
      </div>
      <div className={footerStyle.socialIconBar}>
        <Icon size={{ width: 24, height: 24 }} reverse src={twitterSvg} />
        <Icon size={{ width: 24, height: 24 }} reverse src={linkedinSvg} />
      </div>
    </div>
  </footer>
)

export default Footer
