import * as React from "react"
import { NavigationItem, Logo, LogoText, Button } from "polyvolve-ui"
import { navStyle } from "../lib/reexports"

const Header: React.FunctionComponent = props => (
  <nav className={navStyle.navContainer}>
    <div className={navStyle.nav}>
      <div className={navStyle.logoContainer}>
        <Logo className={navStyle.logoIcon} size={24} />
        <LogoText text="Polyvolve" size={24} />
      </div>
      <ul className={navStyle.navigationBar}>
        <NavigationItem name="Features" />
        <NavigationItem name="Resources" />
      </ul>
      <div className={navStyle.buttonBar}>
        <Button>Contact us</Button>
      </div>
    </div>
  </nav>
)

export default Header
