import * as React from "react"

import { cx, showcaseStyle } from "../lib/reexports"

interface Props {
  innerClassName?: string
  className?: string
  items: ShowcaseItem[]
}

export interface ShowcaseItem {
  head?: JSX.Element
  text: JSX.Element
}

export default class Showcase extends React.Component<Props, any> {
  render(): JSX.Element {
    const { items, innerClassName, className } = this.props

    return (
      <div className={cx(showcaseStyle.showcase, className)}>
        {items.map((item, index) => (
          <div
            className={cx(showcaseStyle.showcaseItem, innerClassName)}
            key={"showcase-item-" + index}>
            {item.head && (
              <div className={showcaseStyle.showcaseItemHead}>{item.head}</div>
            )}
            <div className={showcaseStyle.showcaseItemText}>{item.text}</div>
          </div>
        ))}
      </div>
    )
  }
}
