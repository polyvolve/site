import * as React from "react"
import { floatingShowcaseStyle } from "../lib/reexports"


interface Props {
  items: FloatingShowcaseItem[]
}

export interface FloatingShowcaseItem {
  head: JSX.Element
  text: JSX.Element
}

export default class FloatingShowcase extends React.Component<Props> {
  render(): JSX.Element {
    const { items } = this.props
    return (
      <div className={floatingShowcaseStyle.showcase}>
        {items.map((item, index) => (
          <div className={floatingShowcaseStyle.showcaseItem} key={"showcase-item-" + index}>
            <div className={floatingShowcaseStyle.showcaseItemTitle}>{item.head}</div>
            <div className={floatingShowcaseStyle.showcaseItemText}>{item.text}</div>
          </div>
        ))}
      </div>
    )
  }
}
