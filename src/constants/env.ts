export const DEV = process.env.NODE_ENV !== "production"
export const BASE_PATH = "/site"

export const GA_TRACKING_ID = ""
export const FB_TRACKING_ID = ""
export const SENTRY_TRACKING_ID = ""

export const SITE_NAME = "Polyvolve"
export const SITE_TITLE = "Polyvolve"
export const SITE_DESCRIPTION =
  "Polyvolve helps you quantify the performance of your employees including managers."
export const SITE_IMAGE =
  "https://polyvolve.gitlab.io/site/static/favicon.png"
