import * as React from "react"

import { Layout, Row, RowContent } from "../src/components"
import staticWrapper from "../src/lib/static"

import featuresItems from "../src/constants/features"

import FloatingShowcase from "../src/components/FloatingShowcase"
import { SITE_NAME } from "../src/constants/env"
import { Button } from "polyvolve-ui"
import { indexStyle, displayStyle, cx, globalStyle } from "../src/lib/reexports"

export default () => (
  <Layout>
    <Row outerClassName={cx(indexStyle.headerRow, indexStyle.headerPatternBg)}>
      <RowContent>
        <div className={indexStyle.introductionContainer}>
          <div className={indexStyle.introductionText}>
            <h1 className={indexStyle.introductionTextTitle}>
              A performance review builder and automatic scheduler
            </h1>
            <p>
              {SITE_NAME} streamlines the performance review experience in your
              organization. By leveraging the collected data, you can quantify
              key metrics of employee and manager performance and develop the
              skills of your workforce and your own in the long term.
            </p>
            {/*
            <p>
              {SITE_NAME} enables you to evaluate the performance of
              your company's employees and managers in a statistical way.
            </p>
             */}
          </div>
          <img
            className={displayStyle.introImage}
            src={staticWrapper("/ReviewByClient.jpg")}
          />
        </div>
      </RowContent>
    </Row>
    <Row>
      <RowContent>
        <div className={indexStyle.features}>
          <div className="rowTitle">
            <h1>Features</h1>
          </div>
          <h4>
            Organizing performance reviews is complex and error-prone.{" "}
            {SITE_NAME} automates the tedious details and enables you to work on
            the areas that matter most.
          </h4>
          <FloatingShowcase items={featuresItems} />
        </div>
      </RowContent>
    </Row>
    <Row
      diff
      outerClassName={globalStyle.rowNormal}
      className={indexStyle.newsletterRow}>
      <RowContent>
        <div className={indexStyle.newsletterSignupTitle}>
          <h1>Stay up to date</h1>
          <p>
            {SITE_NAME} currently has alpha status. Sign up to the newsletter to
            stay up to date.
          </p>
        </div>
        <form className={indexStyle.newsletterSignupForm}>
          <input type="email" name="mail" required />
          <Button>Disabled</Button>
        </form>
        <div className={indexStyle.remainingQuestionsContainer}>
          <h4>Open questions?</h4>
          <Button>Contact us</Button>
        </div>
      </RowContent>
    </Row>
  </Layout>
)
